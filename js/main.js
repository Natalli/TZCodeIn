$(document).ready(function () {

    /*animation*/
    if ($(window).outerWidth() >= 768) {
        $('.anLeft').addClass('hidden').viewportChecker({
            classToAdd: 'visible animated fadeInLeftBig',
            offset: 100
        });
        $('.anLeft-2').addClass('hidden').viewportChecker({
            classToAdd: 'visible animated fadeInLeftBig delay-2',
            offset: 100
        });
        $('.anRight').addClass('hidden').viewportChecker({
            classToAdd: 'visible animated fadeInRightBig',
            offset: 100
        });
        $('.anUp').addClass('hidden').viewportChecker({
            classToAdd: 'visible animated fadeInUp',
            offset: 100
        });
        $('.anUp-2').addClass('hidden').viewportChecker({
            classToAdd: 'visible animated fadeInUp delay-2',
            offset: 100
        });
        $('.anUp-3').addClass('hidden').viewportChecker({
            classToAdd: 'visible animated fadeInUp delay-3',
            offset: 100
        });
        $('.anUp-4').addClass('hidden').viewportChecker({
            classToAdd: 'visible animated fadeInUp delay-4',
            offset: 100
        });
    }
});